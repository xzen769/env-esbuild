# Esbuild Env

## Env Install

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v18 use :
```bash
nvm install 18
```

```bash
pnpm i
```

## Usage

Start the application dev with :

```bash
pnpm start
```

Created the build with :

```bash
pnpm build
```

Analyse the coding style  with :

```bash
pnpm pret
```
